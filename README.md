# Fundamentos de Microfrontends: Ejercicio Práctico

## Juego memoria de cartas ##

### Enunciado ###

Desarrollar un juego de cartas donde dado un número inicial este muestre la cantidad multiplicada por 2, es decir, cada carta debe tener un par similar.
Cuando el usuario encuentre las dos cartas similares estas ya no deben mostrarse.

[Prototipo Figma: Ejercicio Práctico 1](https://www.figma.com/design/x9uqJOn72FkfWW0xMFqBvk/Pruebas?node-id=66-47&t=ROcJPDHLFIyKWPZU-0)

### Objetivos ###
- Comprender los fundamentos del desarrollo orientado a componentes.
- Implementación de storybook por cada componente.
- Realizar pruebas unitarias por componente funcional.
- Análisis de prototipos basados en figma.
- Análisis e implementación de patrón vuex.
- Análisis e implementación de clean architecture.

### Consideraciones ###
- Tener instalado NodeJs v16.20.2
- Puede usar los recursos de la carpeta assets/resources o puede usar sus propios recursos