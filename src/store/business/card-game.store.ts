export const CARD_GAME_STORE_GET_CARDS = "CardGameStore/getCards";
export const CARD_GAME_STORE_GET_SELECTED_CARDS = "CardGameStore/getSelectedCards";
export const CARD_GAME_STORE_IS_WINNER = "CardGameStore/isWinner";

export const CARD_GAME_STORE_SET_CARDS = "CardGameStore/setCards";
export const CARD_GAME_STORE_ADD_SELECT_CARD = "CardGameStore/addSelectCard";
export const CARD_GAME_STORE_RESET = "CardGameStore/reset";

import { Card } from "../../core/card-game/domain/card.domain";
import { addSelectCard, isWinner, listCards } from "../../core/card-game/infraestructure/adapters/in/card-game.service";

export default {
    namespaced: true,
    state: {
        cards: [],
        selectedCards: [],
        isWinner: false
    },
    mutations: {
        ADD_SELECT_CARD(state: any, card: Card) {
            state.selectedCards = [...state.selectedCards, card];
            if (state.selectedCards.length === 2) {
                state.cards = addSelectCard(state.selectedCards, state.cards);
                state.selectedCards = [];
            }
            state.isWinner = isWinner(state.cards);
        },
        SET_CARDS(state: any, cards: []) {
            state.cards = cards;
        },
        RESET(state: any) {
            state.cards = [];
            state.selectedCards = [];
        }
    },
    actions: {
        addSelectCard({ commit }: any, card: Card) {
            commit('ADD_SELECT_CARD', card);
        },
        setCards({ commit }: any, size: number) {
            const cardsRandom = listCards(size);
            commit('SET_CARDS', cardsRandom);
        },
        reset({ commit }: any) {
            commit('RESET');
        }
    },
    getters: {
        getCards(state: any) {
            return state.cards;
        },
        getSelectedCards(state: any) {
            return state.selectedCards;
        },
        isWinner(state: any) {
            return state.isWinner;
        }
    }
}