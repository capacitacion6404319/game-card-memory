import { createStore } from 'vuex'

import CardGameStore from './business/card-game.store';

export default createStore({
  modules: {
    CardGameStore
  }
})
