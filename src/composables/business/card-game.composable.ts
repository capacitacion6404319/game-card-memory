import { computed } from "vue";
import { useStore } from "vuex";

import {
    CARD_GAME_STORE_GET_CARDS,
    CARD_GAME_STORE_GET_SELECTED_CARDS,
    CARD_GAME_STORE_IS_WINNER,
    CARD_GAME_STORE_SET_CARDS,
    CARD_GAME_STORE_ADD_SELECT_CARD,
    CARD_GAME_STORE_RESET
} from "../../store/business/card-game.store";

import { executeAction } from "../../utils/vuex.util";

export default function useCardGame() {
    const store = useStore();

    const addSelectCard = (card: any) => {
        executeAction(CARD_GAME_STORE_ADD_SELECT_CARD, card);
    }

    const generateCards = (size: number) => {
        executeAction(CARD_GAME_STORE_SET_CARDS, size);
    }

    const reset = () => {
        executeAction(CARD_GAME_STORE_RESET, null);
    }

    const cards = computed(
        () => store.getters[CARD_GAME_STORE_GET_CARDS]
    );

    const isWinner = computed(
        () => store.getters[CARD_GAME_STORE_IS_WINNER]
    )

    const selectedCards = computed(
        () => store.getters[CARD_GAME_STORE_GET_SELECTED_CARDS]
    )

    return {
        cards,
        isWinner,
        selectedCards,
        addSelectCard,
        generateCards,
        reset
    }
}