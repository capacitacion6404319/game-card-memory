import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

import VwCardsGame from '../views/vw-cards-game/vw-cards-game.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'cards-game',
    component: VwCardsGame
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
