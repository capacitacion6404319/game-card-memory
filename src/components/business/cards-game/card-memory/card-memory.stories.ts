import type { Meta, StoryObj } from '@storybook/vue3';
import { within, userEvent, expect } from '@storybook/test';
import CardMemory from './card-memory.vue';
import { PLANTILLA_HTML } from "../../../../constants/storybook.constant";

const TITULO = `ERP CARD-MEMORY`;

const SUMMARY = `
    El componente <strong>${TITULO}</strong> representa a los cards contenedores personalizados que se especifican en las interfaces presentes en figma, en el se especifícan las siguientes <strong>propiedades</strong>:`;

const PROPS = `
    <li><p class="erp-storybook-texto"><strong>ide</strong>: Number que representa el identificador de un card</p></li>
    <li><p class="erp-storybook-texto"><strong>dotted</strong>: Boolean que representa si el card encontro su par</p></li>
    <li><p class="erp-storybook-texto"><strong>number</strong>: String que representa el número a renderizar en el card</p></li>
    <li><p class="erp-storybook-texto"><strong>type</strong>: Number que representa el tipo de card y estos pueden ser: "Clubs", "Spades", "Heart" y "Diamond"</p></li>
    <li><p class="erp-storybook-texto"><strong>visited</strong>: Boolean que representa si el card si el usuario hizo click sobre el</p></li>
`;

const COMPONENT = `
<div class="component-main" style="margin: 20px 0;">
    <card-memory role="memory-card" :ide="1" :type="args.type" :number="args.number" :dotted="args.dotted" :visited="args.visited">
    </card-memory>
</div>
`

const COMPONENTS = `
<div class="examples">
    <div class="item">
        <card-memory :ide="2" :type="1" :number="'2'" :dotted="false" :visited="false"></card-memory>
    </div>      
    <div class="item">
        <card-memory :ide="3" :type="4" :number="'Q'" :dotted="false" :visited="false"></card-memory>
    </div>    
    <div class="item">
        <card-memory :ide="4" :type="3" :number="'A'" :dotted="false" :visited="false"></card-memory>
    </div> 
    <div class="item">
        <card-memory :ide="5" :type="2" :number="'10'" :dotted="false" :visited="false"></card-memory>
    </div>    
</div>
`;

const AUTOR = `Luis Amat`;

const HISTORY = `
    <span>[2024-05-10][Luis Amat] Creación del componente</span>
`;

const meta: Meta<typeof CardMemory> = {
    title: 'components/business/cards-game/card-memory/card-memory',
    component: CardMemory,
    render: (args: any) => ({
        components: { CardMemory },
        setup() {
            return { args };
        },
        template: PLANTILLA_HTML.replace("$$TITULO$$", `${TITULO}`).replace("$$SUMMARY$$", `${SUMMARY}`).replace("$$PROPS$$", `${PROPS}`).replace("$$COMPONENT$$", `${COMPONENT}`).replace("$$COMPONENTS$$", `${COMPONENTS}`).replace("$$AUTOR$$", `${AUTOR}`).replace("$$HISTORY$$", `${HISTORY}`)
    }),
    parameters: {
        layout: "fullscreen"
    },
    // This component will have an automatically generated docsPage entry: https://storybook.js.org/docs/writing-docs/autodocs
    //tags: ['autodocs'],
};

export default meta;
type Story = StoryObj<typeof CardMemory>;

// More on interaction testing: https://storybook.js.org/docs/writing-tests/interaction-testing
export const CardMemoryBase: Story = {
    args: {
        "ide": 1,
        "type": 1,
        "number": "A",
        "dotted": false,
        "visited": false    
    },
    play: async ({ canvasElement, step }: any) => {
        const canvas = within(canvasElement);
        const cardMemory = canvas.getAllByRole('memory-card');
        const text = cardMemory[0].getElementsByClassName("card-number")[0].textContent;
        await expect(text).toEqual('A');

        await step('Hover CardMemory', async () => {
            await userEvent.hover(cardMemory[0]);
        });
    }
};

CardMemoryBase.storyName = "card-memory";