import { defineComponent, ref } from "vue";

import VCard from "../../../base/v-card/v-card.vue";

import { TypeCardMemoryEnum } from "../../../../enums/type-card-memory.enum";

export default defineComponent({
    inheritAttrs:false,
    components: {
        VCard
    },
    props: {
        ide: {
            type: Number,
            default: 1
        },
        dotted: {
            type: Boolean,
            default: false
        },
        number: {
            type: String,
            default: undefined
        },
        type: {
            type: Number,
            default: 1
        },
        visited: {
            type: Boolean,
            default: false
        }
    },
    setup() {
        const PREFIX = "card-memory";

        return {
            PREFIX,
            TypeCardMemoryEnum
        }
    }
})