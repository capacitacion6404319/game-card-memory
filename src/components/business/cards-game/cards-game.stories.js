import CardsGame from "./cards-game.vue";

export default {
    title: 'components/business/cards-game',
    component: CardsGame,
    //tags: ['autodocs']
}

const Template = args => ({
    components: { CardsGame },
    setup() {
        return { args }
    },    
    //template: PLANTILLA_HTML.replace("$$TITULO$$", `${TITULO}`).replace("$$SUMMARY$$", `${SUMMARY}`).replace("$$PROPS$$", `${PROPS}`).replace("$$COMPONENT$$", `${COMPONENT}`).replace("$$COMPONENTS$$", `${COMPONENTS}`).replace("$$AUTOR$$", `${AUTOR}`).replace("$$HISTORY$$", `${HISTORY}`)
    template: "<cards-game/>"
})

export const CardsGameBase = Template.bind({});
CardsGameBase.storyName = "cards-game";