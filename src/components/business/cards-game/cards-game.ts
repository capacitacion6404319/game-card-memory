import { defineComponent, onMounted, onUnmounted } from "vue";

import CardMemory from "./card-memory/card-memory.vue";

import useCardGame from "../../../composables/business/card-game.composable";

export default defineComponent({
    components: {
        CardMemory
    },
    setup() {
        const PREFIX = "cards-game";

        const { 
            cards,
            isWinner,
            selectedCards, 
            addSelectCard, 
            generateCards, 
            reset 
        } = useCardGame();

        const selectCard = (card: any) => {
            addSelectCard(card);
        }

        onMounted(() => {
            generateCards(5);
        })

        onUnmounted(() => {
            reset();
        })

        return {
            PREFIX,
            isWinner,
            cards,
            selectedCards,
            selectCard
        }
    }
})
