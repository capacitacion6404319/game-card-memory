import { defineComponent } from "vue";

export default defineComponent({
    setup() {
        const PREFIX = "v-card";

        return {
            PREFIX
        }
    }
})
