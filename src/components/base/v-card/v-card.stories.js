import VCard from "./v-card.vue";
import { PLANTILLA_HTML } from "../../../constants/storybook.constant";

const TITULO = `ERP V-CARD`;

const SUMMARY = `
    El componente <strong>${TITULO}</strong> representa a los cards contenedores personalizados que se especifican en las interfaces presentes en figma, en el se especifícan las siguientes <strong>propiedades</strong>:`;

const PROPS = `

`;

const COMPONENT = `
<div class="component-main" style="margin: 20px 0px;">
    <v-card class="box-shadow">
        <template #content>
            
        </template>
    </v-card>
</div>
`

const COMPONENTS = `

`;

const AUTOR = `Luis Amat`;

const HISTORY = `
    <span>[2024-05-10][Luis Amat] Creación del componente</span>
`;

export default {
    title: 'components/base/v-card',
    component: VCard,
    //tags: ['autodocs']
}

const Template = args => ({
    components: { VCard },
    setup() {
        return { args }
    },    
    template: PLANTILLA_HTML.replace("$$TITULO$$", `${TITULO}`).replace("$$SUMMARY$$", `${SUMMARY}`).replace("$$PROPS$$", `${PROPS}`).replace("$$COMPONENT$$", `${COMPONENT}`).replace("$$COMPONENTS$$", `${COMPONENTS}`).replace("$$AUTOR$$", `${AUTOR}`).replace("$$HISTORY$$", `${HISTORY}`)
})

export const VCardBase = Template.bind({});
VCardBase.storyName = "card-base";
VCardBase.parameters = {
    design: {
        type: "figspec",
        accessToken: 'figd_ueHBAkQo1xsbhZ-xaLhyeDtv1JUJmi4tx3OwXIBg',
        url: "https://www.figma.com/design/x9uqJOn72FkfWW0xMFqBvk/Pruebas?node-id=66%3A47&t=nL3ndC8UCycx4eG5-1"
    }
}