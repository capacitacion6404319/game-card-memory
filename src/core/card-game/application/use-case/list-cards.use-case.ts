import { ListCardsPort } from "../../infraestructure/ports/in/list-cards.port";
import { Card } from "../../domain/card.domain";

import { SingletonBase } from "../../../shared/domain/singleton-base.domain";

import cards from "../../../../assets/resources/data-dummy/cards.json";

export class ListCardUseCase extends SingletonBase implements ListCardsPort {

    constructor() {
        super();
    }

    listCards(size: number): Card[] {
        //return Promise.resolve(this.generateCardsRandom(size));
        return this.generateCardsRandom(size);
    }

    private generateCardsRandom(size: number): Card[] {
        if (!cards) {
            return [];
        }
    
        const randomElements = [];
        const copiedArray = [...cards];
    
        while (randomElements.length < size * 2 && copiedArray.length > 0) {
            const randomIndex = Math.floor(Math.random() * copiedArray.length);
            const randomElement = copiedArray.splice(randomIndex, 1)[0];
            randomElements.push(randomElement);      
            randomElements.push({"id": randomElement.id*2, "number": randomElement.number, "type": randomElement.type, "dotted": randomElement.dotted, "visited": randomElement.visited});
        }
    
        return randomElements.sort(() => Math.random() - 0.5);
    }
}