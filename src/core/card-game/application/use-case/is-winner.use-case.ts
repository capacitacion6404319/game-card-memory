import { SingletonBase } from "../../../shared/domain/singleton-base.domain";
import { IsWinnerPort } from "../../infraestructure/ports/in/is-winner.port";
import { Card } from "../../domain/card.domain";

export class IsWinnerUseCase extends SingletonBase implements IsWinnerPort {

    constructor() {
        super();
    }

    isWinner(cards: Card[]): boolean {
        return cards.every( (element) => element.dotted === true);
    }
}