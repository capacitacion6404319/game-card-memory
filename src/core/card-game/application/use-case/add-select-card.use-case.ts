import { SingletonBase } from "../../../shared/domain/singleton-base.domain";
import { AddSelectCardPort } from "../../infraestructure/ports/in/add-select-card.port";
import { Card } from "../../domain/card.domain";

export class AddSelectCardUseCase extends SingletonBase implements AddSelectCardPort {

    constructor() {
        super();
    }

    addSelectCard(selectedCards: Card[], cards: Card[]): Card[] {
        let newCards: Card[] = [];
        if (selectedCards[0].id !== selectedCards[1].id
            && selectedCards[0].number === selectedCards[1].number
            && selectedCards[0].type === selectedCards[1].type
            && selectedCards[0].visited === selectedCards[1].visited) {
            const arr: Card[] = [];
            cards.forEach((x: Card) => {
                arr.push({ ...x });
            });
            arr.forEach((x: Card) => {
                if (x.id === selectedCards[0].id || x.id === selectedCards[1].id) {
                    x.dotted = true
                }
            });
            newCards = [...arr];
        } else {
            newCards = cards;
        }
        return newCards;
    }
}