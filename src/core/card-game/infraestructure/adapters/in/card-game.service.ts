import { Card } from "../../../domain/card.domain";

import { ListCardsPort } from "../../ports/in/list-cards.port";
import { AddSelectCardPort } from "../../ports/in/add-select-card.port";
import { IsWinnerPort } from "../../ports/in/is-winner.port";

import { ListCardUseCase } from "../../../application/use-case/list-cards.use-case";
import { AddSelectCardUseCase } from "../../../application/use-case/add-select-card.use-case";
import { IsWinnerUseCase } from "../../../application/use-case/is-winner.use-case";

export const listCards = (size: number) => {
    const listCardsPort: ListCardsPort = ListCardUseCase.getInstance(ListCardUseCase);
    return listCardsPort.listCards(size);
}

export const addSelectCard = (selectedCards: Card[], cards: Card[] ) => {
    const addSelectCardPort: AddSelectCardPort = AddSelectCardUseCase.getInstance(AddSelectCardUseCase);
    return addSelectCardPort.addSelectCard(selectedCards, cards);
}

export const isWinner = (cards: Card[]) => {
    const isWinnerPort: IsWinnerPort = IsWinnerUseCase.getInstance(IsWinnerUseCase);
    return isWinnerPort.isWinner(cards);
}