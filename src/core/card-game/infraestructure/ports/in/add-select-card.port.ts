import { Card } from "../../../domain/card.domain";

export interface AddSelectCardPort {
    addSelectCard(selectedCards: Card[], cards: Card[]): Card[];
}