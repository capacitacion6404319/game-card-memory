import { Card } from "../../../domain/card.domain";

export interface ListCardsPort {
    listCards(size: number): Card[];
}