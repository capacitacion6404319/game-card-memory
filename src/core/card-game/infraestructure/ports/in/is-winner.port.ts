import { Card } from "../../../domain/card.domain";

export interface IsWinnerPort {
    isWinner(cards: Card[]): boolean;
}