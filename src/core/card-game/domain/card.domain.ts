export class Card {
    id?: number;
    type?: number;
    number?: string;
    dotted?: boolean;
    visited?: boolean;
}