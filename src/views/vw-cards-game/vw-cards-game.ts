import { defineComponent } from "vue";

import CardsGame from "../../components/business/cards-game/cards-game.vue";

export default defineComponent({
    components: {
        CardsGame
    }
})